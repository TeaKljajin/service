@extends('front.layout.master')
@section('content')
    <div style=";height:500px;background-color: #FFF;margin-top: 10%;">
        @if(session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{session('success')}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <a href="{{route('dashboard')}}" class="btn btn-secondary btn-md " role="button" aria-pressed="true" style="float:right;margin-left: 10px;">Go to Companies</a>
        <a href="{{route('logout')}}" class="btn btn-info btn-md active" role="button" aria-pressed="true" style="float:right;">Logout</a>

            <h2>Welcome To Service</h2>
            <h1 style="text-align: center;">Create new Service</h1>

       <div class="row mt-5">
           <div class="col-md-2 mt-5">
               <a href="#" class="btn btn-outline-primary btn-lg active" role="button" aria-pressed="true" style="width: 100%;">Form</a><hr>
               <a href="{{route('service.accepted')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Accepted</a><hr>
               <a href="{{route('service.inProgress')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">In progress</a><hr>
               <a href="{{route('service.onHold')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">On hold</a><hr>
               <a href="{{route('service.finished')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Finished</a><hr>
               <a href="{{route('service.taken')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Taken </a>

           </div>
           <div class="col-md-10">
               <div style="width:65%;margin-left: 15%">
                   <form method="POST" action="{{route('service.store')}}">
                       @csrf
                       <input type="hidden" name="status">
                       <div class="form-group">
                           <select class="form-control" name="user_id">
                               <option>Select User</option>
                               @foreach ($users as $user)
                                   <option value="{{ $user->id }}"> {{ $user->name }} </option>
                               @endforeach
                           </select>
                           @error('user_id')
                           <div style="color:red;">{{ $message }}</div>
                           @enderror
                       </div>

                       <div class="form-group">
                           <label for="date">Date</label>
                           <input type="date" name="date" class="form-control" id="date" aria-describedby="dateHelp">
                           @error('date')
                           <div style="color:red;">{{ $message }}</div>
                           @enderror
                       </div>

                       <div class="form-group">
                           <label for="equipment">Equipment</label>
                           <textarea name="equipment" class="form-control" id="equipment" aria-describedby="equipmentHelp" rows="4" cols="50"  placeholder="Description of equipment received for service"></textarea>
                           @error('equipment')
                           <div style="color:red;">{{ $message }}</div>
                           @enderror
                       </div>
                       <div class="form-group">
                           <label for="serial_no">Serial No</label>
                           <input type="number" name="serial_no" class="form-control" id="serial_no" aria-describedby="serial_noHelp" placeholder="Enter serial number">
                           @error('serial_no')
                           <div style="color:red;">{{ $message }}</div>
                           @enderror
                       </div>
                       <div class="form-group">
                           <label for="note">Note</label>
                           <textarea name="note" class="form-control" rows="4" cols="50" id="note" aria-describedby="noteHelp" placeholder="Note related to the problem with the brought equipment"></textarea>
                           @error('note')
                           <div style="color:red;">{{ $message }}</div>
                           @enderror
                       </div>
                       <button type="submit" class="btn btn-primary">Submit</button>
                   </form>



                   <!-- Button trigger modal -->
                   <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal">
                      Create New User
                   </button> If user not exist create a new one

                   <!-- Modal -->
                   <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                       <div class="modal-dialog" role="document">
                           <div class="modal-content">
                               <div class="modal-header">
                                   <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
                                   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                   </button>
                               </div>
                               <div class="modal-body">
                                   <div class="form-group">
                                       <label for="name">Name</label>
                                       <input type="text" name="name" class="form-control"  placeholder="Enter name">
                                       <span class="text-danger error-text name_error" id="name" ></span>
                                   </div>
                                       <div class="form-group">
                                           <label for="email">Email address</label>
                                           <input type="email" name="email" class="form-control"  placeholder="Enter email">
                                           <span class="text-danger error-text email_error" id="email"></span>
                                       </div>
                                       <div class="form-group">
                                           <label for="password">Password</label>
                                           <input type="password" name="password" class="form-control"  placeholder="Enter password">
                                           <span class="text-danger password_error" id="password"></span>
                                       </div>

                               </div>
                               <div class="modal-footer">
                                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                   <input type="submit" class="btn btn-primary" value="Save" onclick="validation()">
                               </div>

                           </div>
                       </div>
                   </div>



               </div>
           </div>
       </div>
    </div>



@endsection
<script>
    function validation(){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{route('modalStore')}}",
            type: "POST",
            data:{name: $('#name').val(), email: $('#email').val(), password: $('#password').val()},
            beforeSend:function(){
                            $(document).find('span.error-text').text('');
            },
            success:function(data){
                        if(data.status == 0){
                            $.each(data.error,function(prefix, val){
                                $('span.'+prefix+'_error').text(val[0]);
                            });
                        }else{
                              $('#main_form')[0].reset();
                              alert(data.msg);
                        }
                    }

            });


            // done(function(response){
        //     console.log(response.error);
        //     // document.getElementById("name").textContent=response.error.name[0];
        //     $names = document.getElementById("name");
        //     $.each($names, function(){
        //         $(document).find('span.error-text').text(response.error.name);
        //     })
        //     document.getElementById("email").textContent=response.error.email[0];
        //     document.getElementById("password").textContent=response.error.password[0]
        //
        // })

    }




    // $(function(){
    //     $("main_form").on('submit',function(e){
    //         console.log('aaaaaaaaaa');
    //         e.preventDefault();
    //     })

        // $.ajax({
        //     url:$(this).attr('action'),
        //     method:$(this).attr('method'),
        //     data:new FormData(this),
        //     processData:false,
        //     dataType:'json',
        //     contentType:false,
        //     beforeSend:function(){
        //         $(document).find('span.error-text').text('');
        //     },
        //     success:function(data){
        //         if(data.status == 0){
        //             $.each(data.error,function(prefix, val){
        //                 $('span.'+prefix+'_error').text(val[0]);
        //             });
        //         }else{
        //               $('#main_form')[0].reset();
        //               alert(data.msg);
        //         }
        //     }
        //
        // });



    // })

</script>

