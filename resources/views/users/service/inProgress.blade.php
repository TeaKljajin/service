@extends('front.layout.master')
@section('content')
    <div style=";height:500px;background-color: #FFF;margin-top: 10%;">
        @if(session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{session('success')}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <a href="{{route('dashboard')}}" class="btn btn-secondary btn-md " role="button" aria-pressed="true" style="float:right;margin-left: 10px;">Go to Companies</a>
        <a href="{{route('logout')}}" class="btn btn-info btn-md active" role="button" aria-pressed="true" style="float:right;">Logout</a>

        <h2>Dashboard</h2>

        <h1 style="text-align: center;">In Progress</h1>

        <div class="row mt-5">
            <div class="col-md-2 mt-5">
                <a href="{{route('service.index')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Form</a><hr>
                <a href="{{route('service.accepted')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Accepted</a><hr>
                <a href="{{route('service.inProgress')}}" class="btn btn-outline-primary btn-lg active" role="button" aria-pressed="true" style="width: 100%;">In progress</a><hr>
                <a href="{{route('service.onHold')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">On hold</a><hr>
                <a href="{{route('service.finished')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Finished</a><hr>
                <a href="{{route('service.taken')}}" class="btn btn-outline-primary btn-lg" role="button" aria-pressed="true" style="width: 100%;">Taken </a>

            </div>
            <div class="col-md-10">
                <table class="table mt-2">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">User</th>
                        <th scope="col">Equipment</th>
                        <th scope="col">Serial No</th>
                        <th scope="col">Note</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $service)
                        <tr>
                            <th scope="row">{{$services->firstItem()+$loop->index}}</th>
                            <td>{{$service->user->name}}</td>
                            <td>{{Illuminate\Support\Str::limit($service->equipment,10)}}</td>
                            <td>{{$service->serial_no}}</td>
                            <td>{{Illuminate\Support\Str::limit($service->note,10)}}</td>
                            <td>{{$service->created_at->diffForHumans()}}</td>
                            <td>
                                @if($service->status === 2)
                                    <a href="{{route('service.inProgressNotActive',$service->id)}}" class="btn btn-small btn-outline-success">On Hold</a>
                                @endif

                                    @if($service->status === 1 || $service->status === 2 )
                                        <a href="{{route('service.inProgressFinished',$service->id)}}" class="btn btn-small btn-outline-secondary">Finished</a>
                                    @endif
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>
                {{$services->render()}}

            </div>
        </div>

    </div>
@endsection


