@extends('front.layout.master')
@section('content')
    <div style=";height:500px;background-color: #FFF;margin-top: 10%;">
        @if(session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{session('success')}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
            <a href="{{route('service.index')}}" class="btn btn-secondary btn-md " role="button" aria-pressed="true" style="float:right;margin-left: 10px;">Go to Service</a>
            <a href="{{route('logout')}}" class="btn btn-info btn-md active" role="button" aria-pressed="true" style="float:right;">Logout</a>

            <h2>Dashboard</h2>

                <h1 style="text-align: center;">Welcome {{Auth::user()->name}}</h1>
            <a href="{{route('company.create')}}" class="btn btn-outline-success btn-md " role="button" aria-pressed="true" >Create New Company</a>

            <table class="table mt-2">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name of Company</th>
                        <th scope="col">PIB</th>
                        <th scope="col">Address</th>
                        <th scope="col">Contact Person</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $company)
                    <tr>
                        <th scope="row">{{$companies->firstItem()+$loop->index}}</th>
                        <td>{{$company->company_name}}</td>
                        <td>{{$company->pib}}</td>
                        <td>{{$company->street}}, {{$company->number}}, {{$company->city}}</td>
                        <td>{{$company->contact_person}}</td>
                        <td>{{$company->email}}</td>
                        <td>{{$company->phone}}</td>
                        <td><a href="{{route('company.edit',$company->id)}}" class="btn btn-small btn-info">Edit</a>
                            <a href="{{route('company.delete',$company->id)}}" class="btn btn-small btn-danger" onclick="return confirm('Are you sure to delete?')" >Delete</a>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>

                </table>
            {{$companies->render()}}

    </div>
@endsection


