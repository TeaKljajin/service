@extends('front.layout.master')
@section('content')
    <div style=";height:500px;background-color: #FFF;text-align: center;margin-top: 10%;">
        <div class="row">
            <div class="col-md-6">
                <h1>Welcome To Services</h1>
                <a href="{{route('main')}}"><img src="{{asset('images/front/drvo.png')}}" alt=""></a>
            </div>
            <div class="col-md-6">
                <h1>Sign Up</h1>
                <form action="{{route('signUp')}}" method="POST" style="text-align: left;margin-top: 10%">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter name">
                        @error('name')
                        <div style="color:red;">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                        @error('email')
                        <div style="color:red;">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Enter password">
                        @error('password')
                        <div style="color:red;">{{ $message }}</div>
                        @enderror
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
