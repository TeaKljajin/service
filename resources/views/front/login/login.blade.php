@extends('front.layout.master')
@section('content')
    <div style=";height:500px;background-color: #FFF;text-align: center;margin-top: 10%;">
        <div class="row">
            <div class="col-md-6">
                <h1>Welcome To Services</h1>
                <a href="{{route('main')}}"><img src="{{asset('images/front/drvo.png')}}" alt=""></a>
            </div>
            <div class="col-md-6">
                <h1>Sign In</h1>
                <form action="{{route('signIn')}}" method="POST" style="text-align: left;margin-top: 10%">
                    @csrf
                    @if(session('errorMessage'))
                        <p style="color:red;">{{session('errorMessage')}}</p>
                    @endif
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email"  onfocus="this.value=''">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        @error('email')
                        <div style="color:red;">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" onfocus="this.value=''">
                        @error('password')
                        <div style="color:red;">{{ $message }}</div>
                        @enderror
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
