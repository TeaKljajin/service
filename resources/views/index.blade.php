@extends('front.layout.master')
@section('content')
    <div style=";height:500px;background-color: #FFF;text-align: center;margin-top: 10%;">
        @if(session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>{{session('success')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <h1>Welcome To Services</h1>
        <img src="{{asset('images/front/drvo.png')}}" alt="">
        <br>
        <a href="{{route('login')}}" class="btn btn-outline-primary btn-mg" role="button" aria-pressed="true" style="width: 120px;">Login</a>
        <a href="{{route('register')}}" class="btn btn-outline-primary btn-mg" role="button" aria-pressed="true" style="width: 120px;">Register</a>
    </div>
@endsection

