<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Company;

class DashboardController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }

    public function index(){
        $companies = Company::latest()->paginate(2);
        return view('users.dashboard',compact('companies'));
    }


    public function logout(){
        Auth::logout();
        //Session message
        session()->flash('success','You logout successfully');
        //Redirect to
        return redirect('/');

    }
}
