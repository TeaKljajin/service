<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;


class ServiceController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }



    public function index(){
        $users = User::all();
        return view('users.service.index',compact('users'));
    }




    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id'=> 'required|integer',
            'date' => 'required|date_format:Y-m-d|before:tomorrow',
            'equipment' => 'required|string|max:255',
            'serial_no' => 'nullable|digits_between:2,10',
            'note' => 'required|string|max:255',
        ],[
            'user_id.integer' => 'You must choose user',
            'serial_no.digits_between' => 'Serial number must be between 2 and 10 digits'
        ]);

        if ($validator->fails()) {
            return redirect()->route('service.index')
                ->withErrors($validator)
                ->withInput();
        } else {

            Service::insert([
                'user_id' => $request->user_id,
                'created_at' => $request->date,
                'equipment' => $request->equipment,
                'serial_no' => $request->serial_no,
                'note' => $request->note,
                'status' => 1,

            ]);
            //Session message
            session()->flash('success', 'You created successfully new service');
            //Redirect to
            return redirect()->route('service.accepted');
        }
    }


    public function accepted(){
        $services = Service::latest()->paginate(4);
        return view('users.service.accepted',compact('services'));
    }

    public function serviceTaken($id){
        $service = Service::find($id);
        $service->update([
            'status' => 4
        ]);
        //Session message
        session()->flash('success', 'You successfully taken service');
        //Redirect to
        return redirect()->route('service.accepted');
    }

    public function serviceNotTaken($id){
        $service = Service::find($id);
        $service->update([
            'status' => 3
        ]);
        //Session message
        session()->flash('success', 'You restored successfully service from taken');
        //Redirect to
        return redirect()->route('service.accepted');
    }



    public function delete($id){
        //Find Service
        $service = Service::find($id);
        $service->delete();
        //Session message
        session()->flash('success', 'You deleted successfully service');
        //Redirect to
        return redirect()->route('service.accepted');
    }


    public function serviceActive($id){
        $service = Service::find($id);
        $service->update([
            'status' => 2
        ]);
        //Session message
        session()->flash('success', 'You push successfully on service');
        //Redirect to
        return redirect()->route('service.accepted');
    }

    public function serviceNotActive($id){
        $service = Service::find($id);
        $service->update([
            'status' => 1
        ]);
        //Session message
        session()->flash('success', 'You restore successfully from services');
        //Redirect to
        return redirect()->route('service.accepted');
    }


    public function serviceFinished($id){
        $service = Service::find($id);
        $service->update([
            'status' => 3
        ]);
        //Session message
        session()->flash('success', 'You successfully finished service');
        //Redirect to
        return redirect()->route('service.accepted');
    }

    public function serviceNotFinished($id){
        $service = Service::find($id);
        $service->update([
            'status' => 2
        ]);
        //Session message
        session()->flash('success', 'You restored successfully service from finished');
        //Redirect to
        return redirect()->route('service.accepted');
    }

    public function inProgress(){
        $services = Service::where('status',2)->latest()->paginate(3);
        return view('users.service.inProgress',compact('services'));
    }


    public function inProgressNotActive($id){
        $service = Service::find($id);
        $service->update([
            'status' => 1
        ]);
        //Session message
        session()->flash('success', 'You restore successfully from services');
        //Redirect to
        return redirect()->route('service.inProgress');
    }

    public function inProgressFinished($id){
        $service = Service::find($id);
        $service->update([
            'status' => 3
        ]);
        //Session message
        session()->flash('success', 'You successfully finished service on service');
        //Redirect to
        return redirect()->route('service.inProgress');
    }


    public function onHold(){
        $services = Service::where('status','1')->latest()->paginate(4);
        return view('users.service.onHold',compact('services'));
    }


    public function onHoldActive($id){
        $service = Service::find($id);
        $service->update([
            'status' => 2
        ]);
        //Session message
        session()->flash('success', 'You push successfully on service');
        //Redirect to
        return redirect()->route('service.onHold');
    }


    public function onHoldFinished($id){
        $service = Service::find($id);
        $service->update([
            'status' => 3
        ]);
        //Session message
        session()->flash('success', 'You successfully finished service');
        //Redirect to
        return redirect()->route('service.onHold');
    }



    public function finished(){
        $services = Service::where('status',3)->orWhere('status',4)->latest()->paginate(3);
        return view('users.service.finished',compact('services'));
    }

    public function taken(){
        $services = Service::where('status',4)->latest()->paginate(3);
        return view('users.service.taken',compact('services'));
    }

    public function serTaken($id){
        $service = Service::find($id);
        $service->update([
            'status' => 4
        ]);
        //Session message
        session()->flash('success', 'You successfully taken service');
        //Redirect to
        return redirect()->route('service.finished');
    }

    public function serNotTaken($id){
        $service = Service::find($id);
        $service->update([
            'status' => 3
        ]);
        //Session message
        session()->flash('success', 'You successfully restored service from taken');
        //Redirect to
        return redirect()->route('service.finished');
    }

    public function show($id){

        $service = Service::find($id);
        return view('users.service.show',compact('service'));
    }


   //Modal==================
    public function modalStore(Request $request){
//  dd($request);
        $validator = Validator::make($request->all(), [
            'name'=> 'required|min:2|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' =>'required|string|min:6|max:15',

        ],[
            'name.required' => 'The name field is required',
            'name.min' => 'The name must be at list 2 character long'

        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>0,'error'=>$validator->errors()->toArray()]);
        } else {

            $user = User::create([
                'name'=>$request->name,
                'email' => $request->email,
                'password' =>bcrypt($request->password) ,
            ]);

            if($user){
                return response()->json(['status'=>1,'msg'=>'New user has been successfully registered']);
            }
            //Session message
            session()->flash('success', 'You register successfully');
            //Redirect to
            return redirect()->route('service.index');
        }
    }

}


