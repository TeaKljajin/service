<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{

    public function __construct(){
        return $this->middleware('auth');
    }

    public function create(){
       return view('users.company.create');
    }


    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'company_name'=> 'required|string|unique:companies|max:255',
            'pib' => 'required|string|unique:companies|min:9|max:9',
            'street' => 'required|string|max:255',
            'number' => 'required|regex:/^([0-9\s\(\)]*)$/',
            'city' => 'required|string|max:255',
            'contact_person' => 'required|string|max:255',
            'email' => 'nullable|email|max:255',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:9',

        ],[
            'number.required' => 'Required',
            'number.regex' => 'Must be a number',
            'pib.max' => 'The PIB must be 9 character long',
            'pib.min' => 'The PIB must be 9 character long',
            'phone.regex' => 'The field must be a number with min 9 numbers'
        ]);

        if ($validator->fails()) {
            return redirect()->route('company.create')
                ->withErrors($validator)
                ->withInput();
        } else {

             Company::insert([
                 'company_name' => $request->company_name,
                 'pib' => $request->pib,
                 'street' => $request->street,
                 'number' => $request->number,
                 'city' => $request->city,
                 'contact_person' => $request->contact_person,
                 'email' => $request->email,
                 'phone' => $request->phone,
             ]);
            //Session message
            session()->flash('success', 'You created successfully new company');
            //Redirect to
            return redirect()->route('dashboard');
        }
    }


    public function edit($id){
        $company = Company::find($id);
        return view('users.company.edit',compact('company'));
    }



    public function update(Request $request, $id){
        $company = Company::find($id);
        $validator = Validator::make($request->all(), [
            'company_name'=> 'string|max:255|unique:companies,company_name,'.$id,
            'pib' => 'string|min:9|max:9|unique:companies,pib,'.$id,
            'street' => 'string|max:255',
            'number' => 'regex:/^([0-9\s\(\)]*)$/',
            'city' => 'string|max:255',
            'contact_person' => 'string|max:255',
            'email' => 'nullable|email|max:255',
            'phone' => 'regex:/^([0-9\s\-\+\(\)]*)$/|min:9',

        ],[
            'number.required' => 'Required',
            'number.regex' => 'Must be a number',
            'pib.max' => 'The PIB must be 9 character long',
            'pib.min' => 'The PIB must be 9 character long',
            'phone.regex' => 'The field must be a number with min 9 numbers'
        ]);

        if ($validator->fails()) {
            return redirect()->route('company.edit',$id)
                ->withErrors($validator)
                ->withInput();
        } else {
           $category = Company::find($id);
           $category->update([
                'company_name' => $request->company_name,
                'pib' => $request->pib,
                'street' => $request->street,
                'number' => $request->number,
                'city' => $request->city,
                'contact_person' => $request->contact_person,
                'email' => $request->email,
                'phone' => $request->phone,
            ]);
            //Session message
            session()->flash('success', 'You edited successfully company');
            //Redirect to
            return redirect()->route('dashboard');
        }
    }




    public function delete($id){
        $company = Company::find($id);
        $company->delete();
        //Session message
        session()->flash('success', 'You deleted successfully company');
        //Redirect to
        return redirect()->route('dashboard');

    }
}
