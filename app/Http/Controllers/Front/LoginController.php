<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function login(){
        return view('front.login.login');
    }

    public function signIn(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->route('login')
                ->withErrors($validator)
                ->withInput();
        } else {
            //Request data from form
            $credentials = $request->only('email', 'password');
            //Check compatibility
            if (Auth::attempt($credentials)) {
                //Session message
                session()->flash('success', 'You login successfully');
                //Redirect to
                return redirect()->route('dashboard');
            } else {
                return redirect('/login')->with([
                    'errorMessage' => 'You entered wrong login details. Please try again.'
                ]);
            }

        }
    }



    public function register(){
        return view('front.login.register');
    }


    public function signUp(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=> 'required|min:2|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' =>'required|string|min:6|max:15',

        ]);

        if ($validator->fails()) {
            return redirect()->route('register')
                ->withErrors($validator)
                ->withInput();
        } else {

            $user = User::create([
                'name'=>$request->name,
                'email' => $request->email,
                'password' =>bcrypt($request->password) ,

            ]);
            //Login user
            Auth::login($user);
            //Session message
            session()->flash('success', 'You register successfully');
            //Redirect to
            return redirect()->route('dashboard');
        }

    }


}





