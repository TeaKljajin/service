<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
class Service extends Model
{
    use HasFactory;

   protected $fillable = [
       'user_id',
       'equipment',
       'serial_no',
       'note',
       'status'
   ];



    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
